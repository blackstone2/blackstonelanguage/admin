import Layout from '../../Components/Layout'
import PagesIndex from '../../Components/Pages'

export default function Pages() {

    return (
        <Layout>
            <PagesIndex />
        </Layout>
    )
}