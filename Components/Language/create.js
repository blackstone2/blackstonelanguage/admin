import Form from '../forms'
import { AddLanguage } from '../../api';
import { useRouter } from 'next/router';
import ToastMessageDisplay from '../Toast/toastMessageDisplay';

export default function LanguageCreate() {

    const router = useRouter();

    const toBase64 = (file) => {
        return new Promise((resolve, reject) => {
          const fileReader = new FileReader();
      
          fileReader.readAsDataURL(file);
      
          fileReader.onload = () => {
            resolve(fileReader.result);
          };
      
          fileReader.onerror = (error) => {
            reject(error);
          };
        });
    };

    const AddLanguageFunction = async(values) => {
        values.logo = await toBase64(values.logo)
        await AddLanguage(values)
            .then(function(res) {
                
            if(res && res.status == 200) {
                    ToastMessageDisplay({type:"success",message: res.data.message})
                    router.push('/languages')
            } else {
                    ToastMessageDisplay({type:"error",message: res.data.message})
                    

            }
        })
    }

    const formFields = [
        {
            "type" : "TextInput",
            "name" : "name",
            "label" : "Language Name",
            "placeholder" : "Enter Language Name",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "Media",
            "name" : "logo",
            "label" : "Logo",
            "placeholder" : "",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "Dropdown",
            "name" : "status",
            "label" : "Status",
            "placeholder" : "Select Status",
            "validations" : {
                "required" : true,
            },
            "options" : [
                {"key":"Active","value":"Active"},
                {"key":"InActive","value":"InActive"},
            ]
        },
    ];

    return (
        <Form info={formFields} submitAction={AddLanguageFunction}/>
    )
}