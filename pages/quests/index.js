import Layout from '../../Components/Layout'
import DepartmentIndex from '../../Components/Department'

export default function Create() {

    return (
        <Layout>
            <DepartmentIndex />
        </Layout>
    )
}