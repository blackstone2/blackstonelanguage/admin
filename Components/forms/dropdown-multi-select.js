export default function DropdownMultiSelect({info,values,handleChange,touched,errors}) {
    return (
        <div className="col-span-6 sm:col-span-3" key={"Drop"}>
            
            <label htmlFor={info.label} className="block text-sm font-medium text-gray-700">
            {info.label}
            </label>
            <select
                id={info.name}
                name={info.name}
                autoComplete={info.name}
                value={values[info.name]}
                onChange={handleChange}
                multiple
                className={touched[info.name] && errors[info.name] ? "mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm error" : "mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"}
                disabled={info.disabled}
                readonly={info.readonly}
            >{
                <>
                    <option value="">{info.placeholder}</option>
                    {
                        info.options && info.options.map((option) => (
                            <option value={option.value}>{option.key}</option>
                        ))
                    }
                </>
            }
            </select>
            {touched[info.name] && errors[info.name] ? (
                <div className="error-message">{errors[info.name]}</div>
            ) : null}
        </div>
    )
}