import React, { useState, useEffect } from "react";
import { useRouter } from 'next/router';
export default function Blogs() {
    const router = useRouter();

    useEffect(() => {
        if(router.query.token != undefined){
            console.log(router.query.token)
            localStorage.setItem('type',"Teacher");
            localStorage.setItem('token',router.query.token);
            router.push('/courses')
        }
    }, [router.query]);

  return (
    <></>
  );
}