import Layout from '../../Components/Layout'
import DepartmentCreate from '../../Components/Department/create'
export default function Create() {

    return (
        <Layout>
            <DepartmentCreate />
        </Layout>
    )
}