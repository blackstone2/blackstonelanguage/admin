import React, { useEffect, useState } from 'react';
import { Nav, Button } from 'react-bootstrap';
import Link from 'next/link'
import Container from 'react-bootstrap/Container';
import { useRouter } from 'next/router';
import { GetCountries } from '../../api';

export default function Navbar() {

    const router = useRouter()

    const [isUserLoggedIn,setIsUserLoggedIn] = useState(false);
    const [userType,setUserType] = useState("Admin");
    const [databases,setDatabases] = useState([]);
    const [selectedDatabase,setSelectedDatabase] = useState([]);
    const [showSidebar,setShowSidebar] = useState(false);

    useEffect(() => {   
        if(localStorage.getItem('token') != undefined){
            setIsUserLoggedIn(true);
            setUserType(localStorage.getItem('type'))
        }
        else{
            setIsUserLoggedIn(false);
        }
        
    },[])

    useEffect(() => {   
        const fetchData = async() => {
            await GetCountries()
            .then(function(res) {
                if(res && res.status == 200) {
                    console.log(res.data.databases)
                    setDatabases(res.data.databases);
                    setSelectedDatabase(localStorage.getItem('country'))
                } else {
                    
                }
            })
        }
        fetchData()
    },[])

    const handleLogout = () => {
        localStorage.clear();
        router.push('/auth/login');
    }

    const handleToggle = () => {
        setShowSidebar(!showSidebar);
    }

    const handleDbChange = (e) => {
        console.log(e.target.value)
        localStorage.setItem('country',e.target.value);
        setSelectedDatabase(e.target.value)
        window.location.reload();
    }

    return (
        
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark row">
            <div className="container-xl ps-4">
                <Link href="/" className="navbar-brand">Cair 4 YOUTH
                </Link>
                <button onClick={handleToggle} className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                {
                    isUserLoggedIn ? (
                
                        <><div className={showSidebar ? "collapse navbar-collapse show" : "collapse navbar-collapse"} id="navbarsExample07XL">
                            <ul className="navbar-nav mr-auto">
                                <li className="nav-item active">
                                    <Link href="/" className="nav-link">Home
                                    </Link>
                                </li>
                                {userType && (userType == "Admin" || userType == "Super Admin") ?
                                    (
                                        <>
                                            {userType == "Super Admin" || userType == "Admin" ?
                                                (
                                                    <li className="nav-item">
                                                        <Link href="/users" className="nav-link">Users
                                                        </Link>
                                                    </li>
                                                ) : (null)}
                                            <li className="nav-item">
                                                <Link href="/settings" className="nav-link">Settings
                                                </Link>
                                            </li>
                                            <li className="nav-item">
                                                <Link href="/pages" className="nav-link">Pages
                                                </Link>
                                            </li>

                                            {userType == "Super Admin" ?
                                                (
                                                    <li className="nav-item">
                                                        <Link href="/languages" className="nav-link">Languages
                                                        </Link>
                                                    </li>
                                                ) : (null)}
                                            <li className="nav-item">
                                                <Link href="/quests" className="nav-link">Quests
                                                </Link>
                                            </li>
                                        </>
                                    ) : (null)}
                                <li className="nav-item">
                                    <Link href="/courses" className="nav-link">Courses
                                    </Link>
                                </li>


                                <li className="nav-item">
                                    <a onClick={handleLogout} style={{ cursor: "pointer" }} className="nav-link">Logout
                                    </a>
                                </li>
                            </ul>
                        </div></>
                    ) : (null)
                }
            </div>
        </nav>
        
    )
}