import Layout from '../../Components/Layout'
import DepartmentEdit from '../../Components/Department/edit'

export default function Create() {
    
    return (
        <Layout>
            <DepartmentEdit />
        </Layout>
    )
}