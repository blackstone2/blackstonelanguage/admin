/** @type {import('next').NextConfig} */
const withImages = require('next-images')

const nextConfig = {
  
  reactStrictMode: true,
  env: {
    protocol: 'https://',
    baseUrl: 'da56-45-118-105-22.ngrok-free.app/',
  },
}

module.exports = withImages(nextConfig)
