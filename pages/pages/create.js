import Layout from '../../Components/Layout'
import PagesCreate from '../../Components/Pages/create'
export default function Create() {

    return (
        <Layout>
            <PagesCreate />
        </Layout>
    )
}