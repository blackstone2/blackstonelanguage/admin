import Form from './forms'
import { saveSetting, GetSetting } from './../api';
import { useRouter } from 'next/router';
import ToastMessageDisplay from './Toast/toastMessageDisplay';
import React, { useEffect, useState } from 'react';

export default function Create() {

    const router = useRouter()

    const [initialData,setInitialData] = useState([]);
    const [formFields,setFormFields] = useState([
        {
            "type" : "TextInput",
            "name" : "color",
            "label" : "Color Code ",
            "placeholder" : "Enter Color Code (#000000)",
        },
        {
            "type" : "Media",
            "name" : "logo",
            "label" : "Logo",
            "placeholder" : "",
        },
        {
            "type" : "Media",
            "name" : "language",
            "label" : "Language Json",
            "placeholder" : "",
        }
    ]);


    const toBase64 = (file) => {
        return new Promise((resolve, reject) => {
          const fileReader = new FileReader();
      
          fileReader.readAsDataURL(file);
      
          fileReader.onload = () => {
            resolve(fileReader.result);
          };
      
          fileReader.onerror = (error) => {
            reject(error);
          };
        });
    };
    useEffect(() => {   
        const fetchData = async() => {
            await GetSetting()
            .then(function(res) {
                if(res && res.status == 200) {
                    setInitialData(res.data.settings)
                } else {
                    
                }
            })
        }
        
        fetchData();
        if (typeof window !== 'undefined') {

            if(localStorage.getItem('type') == "Admin"){
                const fields = formFields.filter((fd) => fd.name != "language")
                setFormFields(fields)
                console.log("formFields",formFields)
            }
        }
        
    },[])

    const UpdateSettingFunction = async(values) => {

        console.log(values.logo);
        console.log(values.language);
        if(values.logo != undefined && typeof values.logo == 'object')
            values.logo = await toBase64(values.logo)
        if(values.language != undefined && typeof values.language == 'object')
            values.language = await toBase64(values.language)
        await saveSetting(values)
            .then(function(res) {
                
            if(res && res.status == 200) {
                    ToastMessageDisplay({type:"success",message: res.data.message})
                    router.push('/settings')
            } else {
                    ToastMessageDisplay({type:"error",message: res.data.message})

            }
        });
        
    }

    

    

    return (
        <Form info={formFields} submitAction={UpdateSettingFunction} getInitialData={initialData} />
    )
}