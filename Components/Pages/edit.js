import Form from '../forms'
import { UpdatePage, GetPage } from '../../api';
import { useRouter } from 'next/router';
import ToastMessageDisplay from '../Toast/toastMessageDisplay';
import React, { useEffect, useState } from 'react';

export default function Create() {

    const router = useRouter()

    const [initialData,setInitialData] = useState([])

    
    useEffect(() => {   
        const fetchData = async(id) => {
            await GetPage(id)
            .then(function(res) {
                if(res && res.status == 200) {
                    setInitialData(res.data.page)
                } else {
                    
                }
            })
        }
        if(router && router.query && router.query.id)
        {
            fetchData(router.query.id)
        }
    },[router.query])

    const UpdatePageFunction = async(values) => {

        if(router.query && router.query.id)
        {
            await UpdatePage(values,router.query.id)
                .then(function(res) {
                    
                if(res && res.status == 200) {
                        ToastMessageDisplay({type:"success",message: res.data.message})
                        router.push('/pages')
                } else {
                        ToastMessageDisplay({type:"error",message: res.data.error})

                }
            })
        }
    }

    const formFields = [
        {
            "type" : "TextInput",
            "name" : "name",
            "label" : "Name",
            "placeholder" : "Enter Name",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "TextInput",
            "name" : "slug",
            "label" : "Slug",
            "placeholder" : "Enter Slug",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "TextInputNumber",
            "name" : "sequence",
            "label" : "Sequence Number",
            "placeholder" : "Enter Sequence Number",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "Dropdown",
            "name" : "status",
            "label" : "Status",
            "placeholder" : "Select Status",
            "validations" : {
                "required" : true,
            },
            "options" : [
                {"key":"Active","value":"Active"},
                {"key":"InActive","value":"InActive"},
            ]
        },
        {
            "type" : "TextArea",
            "name" : "content",
            "label" : "Content",
            "placeholder" : "Content",
            "validations" : {
                "required" : true,
            },
        },
        
    ];

    return (
        <Form info={formFields} submitAction={UpdatePageFunction} getInitialData={initialData} />
    )
}