import Form from '../forms'
import { UpdateUser, GetUser } from '../../api';
import { useRouter } from 'next/router';
import ToastMessageDisplay from '../Toast/toastMessageDisplay';
import React, { useEffect, useState } from 'react';

export default function Create() {

    const router = useRouter()

    const [initialData,setInitialData] = useState([])

    
    useEffect(() => {   
        const fetchData = async(id) => {
            await GetUser(id)
            .then(function(res) {
                if(res && res.status == 200) {
                    setInitialData(res.data.user)
                } else {
                    
                }
            })
        }
        if(router && router.query && router.query.id)
        {
            fetchData(router.query.id)
        }
    },[router.query])

    const UpdateUserFunction = async(values) => {

        if(router.query && router.query.id)
        {
            await UpdateUser(values,router.query.id)
                .then(function(res) {
                    
                if(res && res.status == 200) {
                        ToastMessageDisplay({type:"success",message: res.data.message})
                        router.push('/users')
                } else {
                        ToastMessageDisplay({type:"error",message: res.data.error})

                }
            })
        }
    }

    const formFields = [
        {
            "type" : "TextInput",
            "name" : "name",
            "label" : "Name",
            "placeholder" : "Enter Name",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "TextInput",
            "name" : "email",
            "label" : "Email",
            "placeholder" : "Enter Email",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "Dropdown",
            "name" : "type",
            "label" : "User Type",
            "placeholder" : "Select Type",
            "validations" : {
                "required" : true,
            },
            "options" : [
                {"key":"Super Admin","value":"Super Admin"},
                {"key":"Admin","value":"Admin"},
                {"key":"Teacher","value":"Teacher"},
                {"key":"Student","value":"Student"},
            ]
        },
        {
            "type" : "Dropdown",
            "name" : "status",
            "label" : "Status",
            "placeholder" : "Select Status",
            "validations" : {
                "required" : true,
            },
            "options" : [
                {"key":"Active","value":"Active"},
                {"key":"InActive","value":"InActive"},
            ]
        },
        
    ];

    return (
        <Form info={formFields} submitAction={UpdateUserFunction} getInitialData={initialData} />
    )
}