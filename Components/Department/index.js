import { GetDepartments, DeleteDepartment} from '../../api';
import TableWithAction from '../Datatable/table';
import React, { useEffect, useState } from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { useRouter } from 'next/router';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import ToastMessageDisplay from '../Toast/toastMessageDisplay';

export default function DepartmentIndex() {

    const [departments,setDepartments] = useState([]);
    const router = useRouter()
    const [type,setType] = useState(false)
    
    const departmentHeader = [
        {"header_name" : "Quests Name","key_value" : "name"},
        {"header_name" : "Language","key_value" : "language"},
        {"header_name" : "Status","key_value" : "status"}
    ];
    
    const fetchDepartment = async(page = 1) => {
        await GetDepartments(page)
        .then(function(res) {
            if(res && res.status == 200) {
                setDepartments(res.data.details)
            } else {
                
            }
        })
    }

    const deleteDepartment = async(id) => {
        console.log(id);
        await DeleteDepartment(id)
        .then(function(res) {
            if(res && res.status == 200) {
                ToastMessageDisplay({type:"success",message: res.data.message})
                fetchDepartment();
            } else {
                
            }
        })
    }

    useEffect(() => {
        fetchDepartment()
        if (typeof window !== 'undefined') {

            if(localStorage.getItem('type') == "Super Admin")
            {
                setType(false)
            }
            else{
                setType(true)
            }
        }
        
    },[])

    const actions = [
        {
            action_type : "redirection",
            action_name : "Edit",
            pre_url : "/quests/edit/"
        },
        {
            action_type : "action_with_popup",
            action_name : "Delete",
            action_function : deleteDepartment
        }
    ];


    return (

        <Container style={{marginTop:"20px"}}>
            <Row>
                
                <Col>
                    <h5>Quests details</h5>
                </Col>
                {
                    !type ? (
                        <Col className="pull-right">
                            <Button onClick={() => {
                                router.push('quests/create')
                            }} style={{float:'right'}}>Add new</Button>
                        </Col>
                    ) : (null)
                }
                
                    
                
            </Row>

            <Row >
                
                <TableWithAction header={departmentHeader} data={departments} actions={actions} onPageChangeFunction={fetchDepartment}/>
                
            </Row>

            
            
            {/* <DepartmentCreate /> */}
        </Container>

    )
}