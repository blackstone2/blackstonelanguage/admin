import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';


export default function InputText({info,values,handleChange,touched,errors}) {
  return (
      info.visible_condition && Object.keys(info.visible_condition).length > 0 ?
      (
        <>
        {
          values[info.visible_condition.dependent_column] == info.visible_condition.expected_column_value ?
          (
            <Col md={6}>
              <Form.Label htmlFor={info.label}>
              {info.label}
              </Form.Label>
              <Form.Control
              
              type="text"
              name={info.name}
              placeholder={info.placeholder}
              value={values[info.name]}
              id={info.label}
              onChange={handleChange}
              autoComplete={info.name}
              
              disabled={info.disabled}
              readOnly={info.readonly}
              />
              {touched[info.name] && errors[info.name] ? (
                  <div className="error-message">{errors[info.name]}</div>
              ) : null}
            </Col>
          )
          :
          (
            null
          )
        }
          
        </>
      )
      :
      (
        
        <Col md={6}>
          <Form.Label htmlFor={info.label}>
          {info.label}
          </Form.Label>
          <Form.Control
          type="text"
          name={info.name}
          placeholder={info.placeholder}
          value={values[info.name]}
          id={info.label}
          onChange={handleChange}
          autoComplete={info.name}
          
          disabled={info.disabled}
          readOnly={info.readonly}
          />
          {touched[info.name] && errors[info.name] ? (
              <div className="error-message">{errors[info.name]}</div>
          ) : null}
        </Col>
      )
      
    
  )
}