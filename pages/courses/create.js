import Layout from '../../Components/Layout'
import CourseCreate from '../../Components/Course/create'
export default function Create() {

    return (
        <Layout>
            <CourseCreate />
        </Layout>
    )
}