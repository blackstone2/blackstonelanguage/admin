import Layout from '../../Components/Layout'
import LanguageEdit from '../../Components/Language/edit'

export default function Create() {
    
    return (
        <Layout>
            <LanguageEdit />
        </Layout>
    )
}