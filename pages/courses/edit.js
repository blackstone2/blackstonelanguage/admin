import Layout from '../../Components/Layout'
import CourseEdit from '../../Components/Course/edit'

export default function Create() {
    
    return (
        <Layout>
            <CourseEdit />
        </Layout>
    )
}