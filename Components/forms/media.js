import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';

export default function Media({info,values,handleChange,touched,errors,setFieldValue}) {

    const toBase64 = profile_picture => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(profile_picture);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
    return (

        <Col md={6}>
          <Form.Label htmlFor={info.label}>
          {info.label}
          </Form.Label>
          <Form.Control
          
            name={info.name}
            type="file" 
            id={info.label}
            disabled={info.disabled}
            readonly={info.readonly}
            onChange={async(event) => {
                setFieldValue(info.name, event.currentTarget.files[0]);
            }}
          />
          {touched[info.name] && errors[info.name] ? (
              <div className="error-message">{errors[info.name]}</div>
          ) : null}

          {
            values[info.name] ?
            (
                <a href={`${values[info.name]}`} target="_blank" download >Download</a>
            ) :
            (null)
          }
        </Col>
        
    )
}