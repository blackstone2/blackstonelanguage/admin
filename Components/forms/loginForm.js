import TextInput from './input-text';
import TextArea from './textarea';
import Dropdown from './dropdown';
import DropdownMultiSelect from './dropdown-multi-select';
import DependantDropdown from './DependantDropdown';
import TextInputPassword from './input-text-password'
import RadioGroup from './RadioGroup';
import InputTextNumber from './InputTextNumber';
import Media from './media';
import { Formik, Form, Field, FieldArray, ErrorMessage } from 'formik';
import React, { useEffect, useState } from 'react';
import * as Yup from 'yup';
import toast from "../Toast";
import { useRouter } from 'next/router';
import { Button } from 'flowbite-react';

export default function FormComponent({ info,submitAction,getInitialData }) {

    const [initialValues,setInitialValues] = useState({});
    const [validationSchema,setValidationSchema] = useState({});
    const router = useRouter()

    const notify = React.useCallback((type, message) => {
        toast({ type, message });
    }, []);
    

    const fetchData = async() => {
        let initValues = {};
        let validation = {};
        info.map((field) => {

            initValues[field.name] = getInitialData[field.name];

            if(field.type == "DropdownMultiSelect")
            {
                let arr = []
                getInitialData[field.name].map((value) => {
                    arr.push(value.table_name)
                })
                initValues[field.name] = arr
            }
            if(field.validations && field.validations.required){
                validation[field.name] = Yup.string().required('Required')
            }
        })
        setInitialValues(initValues);

        const validationSchema = Yup.object().shape(validation);
        setValidationSchema(validationSchema);
    }
    useEffect(() => {   
        if(getInitialData && Object.keys(getInitialData).length > 0)
        {
            fetchData()
        }
        else{
            let initValues = {};
            let validation = {};
            info.map((field) => {
                initValues[field.name] = "";
                if(field.validations && field.validations.required){
                    validation[field.name] = Yup.string().required('Required')
                }
            })
            setInitialValues(initValues);

            const validationSchema = Yup.object().shape(validation);
            setValidationSchema(validationSchema);
        }
        
    },[getInitialData]);    
    
    return (
        
        <div className="relative flex flex-col justify-center min-h-screen overflow-hidden">
        <div className="w-full p-6 m-auto bg-white rounded-md shadow-xl shadow-rose-600/40 ring ring-2 ring-orange-300 lg:max-w-xl">
            <h1 className="text-3xl font-semibold text-center text-orange-700 underline uppercase decoration-wavy">
                   <center><img src="/logo.jpg" alt="image" className="w-16 md:w-32 lg:w-48"/></center>
                </h1>

                    <Formik
                        enableReinitialize="true"
                        initialValues={initialValues}
                        validationSchema={validationSchema}
                        onSubmit={async (values, { resetForm }) => {
                            submitAction(values)
                        }}
                    >
                        {({ handleSubmit, handleChange, values, setFieldValue, errors, touched }) => (
                            <form onSubmit={handleSubmit}>
                                <div className="shadow sm:rounded-md bg-white space-y-6 space-x-6 sm:p-6" key={"rounded"}>
                                    <div className="">
                                        {
                                            info.map((element) => (
                                                <>
                                                   
                                                        {
                                                            element.type == "TextInput" ?
                                                                <TextInput info={element} values={values} handleChange={handleChange} touched={touched} errors={errors}  />
                                                                : null
                                                        }
                                                   
                                                   
                                                    {
                                                        element.type == "TextInputPassword" ?
                                                            <TextInputPassword info={element} values={values} handleChange={handleChange} touched={touched} errors={errors}  />
                                                            : null
                                                    }   
                                                </>
                                            ))
                                        }
                                    </div>
                                </div>
                                    <div className="mt-6">
                                        <center>
                                            <button className="submit-button w-1/2 px-4 py-2 tracking-wide text-white transition-colors duration-200 transform  rounded-md  focus:outline-none" onClick={handleSubmit}>
                                                Login
                                            </button>
                                        </center>
        
                                    </div>
                            </form>
                        )}
                    </Formik>
            </div>
        </div>
    )
}