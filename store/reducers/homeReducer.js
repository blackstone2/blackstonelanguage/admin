const initialState = { 
    data: [] 
}

const HomeReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'HomePage':
        return {
          ...state,
          data : action.data
        }
      default:
        return state
    }
}

export default HomeReducer