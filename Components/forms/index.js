import TextInput from './input-text';
import TextArea from './textarea';
import Dropdown from './dropdown';
import DropdownMultiSelect from './dropdown-multi-select';
import DependantDropdown from './DependantDropdown';
import TextInputPassword from './input-text-password'
import RadioGroup from './RadioGroup';
import InputTextNumber from './InputTextNumber';
import Media from './media';
import InputCKEditor from './InputCKEditor';
import { Formik, Form, Field, FieldArray, ErrorMessage } from 'formik';
import React, { useEffect, useState, useRef } from 'react';
import * as Yup from 'yup';
import toast from "../Toast";
import { useRouter } from 'next/router';
import { Button } from 'react-bootstrap';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';


export default function FormComponent({ info,submitAction,getInitialData,type="" }) {

    const [initialValues,setInitialValues] = useState({});
    const [validationSchema,setValidationSchema] = useState({});
    const router = useRouter()

    let quillData = [];


    const notify = React.useCallback((type, message) => {
        toast({ type, message });
    }, []);
    

    const fetchData = async() => {
        let initValues = {};
        let validation = {};
        info.map((field) => {

            initValues[field.name] = getInitialData[field.name];
            console.log(field.type)
            if(field.type == "DropdownMultiSelect")
            {
                let arr = []
                getInitialData[field.name].map((value) => {
                    arr.push(value.table_name)
                })
                initValues[field.name] = arr
            }
            if(field.validations && field.validations.required && field.type != "TextArea"){
                validation[field.name] = Yup.string().required('Required')
            }
        })
        setInitialValues(initValues);

        const validationSchema = Yup.object().shape(validation);
        setValidationSchema(validationSchema);
    }
    useEffect(() => {   
        if(getInitialData && Object.keys(getInitialData).length > 0)
        {
            fetchData()
        }
        else{
            let initValues = {};
            let validation = {};
            info.map((field) => {
                initValues[field.name] = "";
                if(field.validations && field.validations.required && field.type != "TextArea"){
                    validation[field.name] = Yup.string().required('Required')
                }
            })
            setInitialValues(initValues);

            const validationSchema = Yup.object().shape(validation);
            setValidationSchema(validationSchema);
        }
        
    },[getInitialData]);    
    
    return (

        
        <Container style={{marginTop:"20px"}}>
            
                
            <Formik
                enableReinitialize="true"
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={async (values, { resetForm }) => {

                    console.log(quillData);
                    const updates = Object.keys(quillData);
                    updates.forEach((update) => values[update] = quillData[update])
                    submitAction(values)
                }}
            >
                {({ handleSubmit, handleChange, values, setFieldValue, errors, touched }) => (
                    <form onSubmit={handleSubmit}>
                        <Row>
                            
                                {
                                    info.map((element) => (
                                        <>
                                            {
                                                element.type == "TextInput" ?
                                                    <TextInput info={element} values={values} handleChange={handleChange} touched={touched} errors={errors}  />
                                                    : null
                                            }
                                            {
                                                element.type == "TextInputPassword" ?
                                                    <TextInputPassword info={element} values={values} handleChange={handleChange} touched={touched} errors={errors}  />
                                                    : null
                                            }
                                            {
                                                element.type == "TextArea" ?
                                                    <InputCKEditor info={element} values={values} handleChange={handleChange} touched={touched} errors={errors}  setFieldValue={setFieldValue} quillData={quillData}/>
                                                    : null
                                            }
                                            {
                                                element.type == "BasicTextArea" ?
                                                    <TextArea info={element} values={values} handleChange={handleChange} touched={touched} errors={errors} setFieldValue={setFieldValue} />
                                                    : null
                                            }
                                            {
                                                element.type == "Dropdown" ?
                                                    <Dropdown info={element} values={values} handleChange={handleChange} touched={touched} errors={errors} />
                                                    : null
                                            }
                                            {
                                                element.type == "DependantDropdown" ?
                                                    <DependantDropdown info={element} values={values} handleChange={handleChange} touched={touched} errors={errors} />
                                                    : null
                                            }
                                            {
                                                element.type == "DropdownMultiSelect" ?
                                                    <DropdownMultiSelect info={element} values={values} handleChange={handleChange} touched={touched} errors={errors} />
                                                    : null
                                            }
                                            {
                                                element.type == "Media" ?
                                                    <Media info={element} values={values} handleChange={handleChange} touched={touched} errors={errors} setFieldValue={setFieldValue}/>
                                                    : null
                                            }
                                            {
                                                element.type == "RadioGroup" ?
                                                    <RadioGroup info={element} values={values} handleChange={handleChange} touched={touched} errors={errors} />
                                                    : null
                                            }
                                            {
                                                element.type == "TextInputNumber" ?
                                                    <InputTextNumber info={element} values={values} handleChange={handleChange} touched={touched} errors={errors} />
                                                    : null
                                            }
                                        </>


                                    ))
                                }
                            
                        </Row>
                        <Row style={{marginTop:"10px"}}>
                            <Col md={2}>
                                <span class="pull-left">
                                    <Button 
                                        onClick={handleSubmit}>
                                    <span class="flex items-center rounded-md text-sm px-4 py-2">Save</span></Button>
                                </span>
                            </Col>
                            <Col md={2}>
                                <span class="pull-right"> 
                                    <Button
                                        onClick={() => {
                                        router.back()
                                    }}>
                                    <span class="flex items-center rounded-md text-sm px-4 py-2">Back</span></Button>
                                </span>
                            </Col>

                        </Row>
                        
                    </form>
                )}
            </Formik>
            
            
            
        </Container>

    )
}