import Form from '../forms'
import { AddDepartment, GetLanguages } from '../../api';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import ToastMessageDisplay from '../Toast/toastMessageDisplay';

export default function DepartmentCreate() {

    const router = useRouter()
    const [type,setType] = useState(false)

    const toBase64 = (file) => {
        return new Promise((resolve, reject) => {
          const fileReader = new FileReader();
      
          fileReader.readAsDataURL(file);
      
          fileReader.onload = () => {
            resolve(fileReader.result);
          };
      
          fileReader.onerror = (error) => {
            reject(error);
          };
        });
    };
    const [options,setOptions] = useState([])

    const AddDepartmentFunction = async(values) => {
        values.logo = await toBase64(values.logo)
        await AddDepartment(values)

            .then(function(res) {
                
            if(res && res.status == 200) {
                    ToastMessageDisplay({type:"success",message: res.data.message})
                    router.push('/quests')
            } else {
                    ToastMessageDisplay({type:"error",message: res.data.message})
                    

            }
        })
    }

    useEffect(() => {   
        const fetchData = async() => {
            await GetLanguages()
            .then(function(res) {
                if(res && res.status == 200) {
                    let arr = [];
                    
                    res.data.details.map((lang) => {
                        arr.push({"key":lang.name ,"value":lang.name})
                        setOptions(arr)
                        
                    })
                } else {
                    
                }
            })
        }
        
        fetchData()

        if (typeof window !== 'undefined') {

            if(localStorage.getItem('type') == "Super Admin")
            {
                setType(false)
            }
            else{
                setType(true)
            }
        }
        
    },[])

    const formFields = [
        {
            "type" : "TextInput",
            "name" : "name",
            "label" : "Quests Name",
            "placeholder" : "Enter Quests Name",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "Dropdown",
            "name" : "language",
            "label" : "Language Name",
            "placeholder" : "Select Language Name",
            "validations" : {
                "required" : true,
            },
            "options" : options,
            "disabled" : type
        },
        {
            "type" : "Media",
            "name" : "logo",
            "label" : "Logo",
            "placeholder" : "",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "Dropdown",
            "name" : "status",
            "label" : "Status",
            "placeholder" : "Select Status",
            "validations" : {
                "required" : true,
            },
            "options" : [
                {"key":"Active","value":"Active"},
                {"key":"InActive","value":"InActive"},
            ]
        },
        
    ];

    return (
        <Form info={formFields} submitAction={AddDepartmentFunction}/>
    )
}