// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import axiosInstance from "../services";
import axiosInstanceMedia from "../services/axiosMedia";



export const Login = (data) => {
  return axiosInstance.post(`/login`, data);
};

export const GetUsers = () => {
  return axiosInstance.get(`/users`);
};

export const AddUser = (data) => {
  return axiosInstance.post(`/save-user`,data);
};

export const GetUser = (id) => {
  return axiosInstance.get(`/user/${id}`);
};

export const UpdateUser = (data,id) => {
  return axiosInstance.post(`/update-user/`+id,data);
};

export const Activate = (id) => {
  return axiosInstance.get(`/activate-user/${id}`);
};



export const DeleteUser = (id) => {
  return axiosInstance.delete(`/user/${id}`);
};


export const GetLanguages = () => {
  return axiosInstance.get(`/languages`);
};


export const DeleteLanguage = (id) => {
  return axiosInstance.delete(`/language/${id}`);
};

export const AddLanguage = (data) => {
  return axiosInstance.post(`/save-language`,data);
};

export const getLanguage = (id) => {
  return axiosInstance.get(`/language/${id}`);
};

export const UpdateLanguage = (data,id) => {
  return axiosInstance.post(`/update-language/`+id,data);
};


export const GetDepartments = () => {
  return axiosInstance.get(`/departments`);
};


export const DeleteDepartment = (id) => {
  return axiosInstance.delete(`/department/${id}`);
};

export const AddDepartment = (data) => {
  return axiosInstance.post(`/save-department`,data);
};

export const GetDepartment = (id) => {
  return axiosInstance.get(`/department/${id}`);
};

export const UpdateDepartment = (data,id) => {
  return axiosInstance.post(`/update-department/`+id,data);
};


export const GetCourses = () => {
  return axiosInstance.get(`/courses`);
};

export const DeleteCourse = (id) => {
  return axiosInstance.delete(`/course/${id}`);
};

export const AddCourse = (data) => {
  return axiosInstance.post(`/save-course`,data);
};

export const GetCourse = (id) => {
  return axiosInstance.get(`/course/${id}`);
};

export const UpdateCourse = (data,id) => {
  return axiosInstance.post(`/update-course/`+id,data);
};


export const saveSetting = (data,id) => {
  return axiosInstance.post(`/setting/`,data);
};


export const GetSetting = () => {
  return axiosInstance.get(`/setting`);
};

export const ImageUploadEditor = (data) => {
  return axiosInstance.post(`/save-image-editor`,data);
};


export const GetPages = () => {
  return axiosInstance.get(`/pages`);
};

export const AddPage = (data) => {
  return axiosInstance.post(`/save-page`,data);
};

export const GetPage = (id) => {
  return axiosInstance.get(`/page/${id}`);
};

export const UpdatePage = (data,id) => {
  return axiosInstance.post(`/update-page/`+id,data);
};

export const DeletePage = (id) => {
  return axiosInstance.delete(`/page/${id}`);
};

export const GetCountries = () => {
  return axiosInstance.get(`/get-countries`);
};


