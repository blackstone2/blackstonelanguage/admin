import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';

export default function TextInputPassword({info,values,handleChange,touched,errors}) {
return (
    <Col md={6}>
          <Form.Label htmlFor={info.label}>
          {info.label}
          </Form.Label>
          <Form.Control
          type="password"
          name={info.name}
          placeholder={info.placeholder}
          value={values[info.name]}
          id={info.label}
          onChange={handleChange}
          autoComplete={info.name}
          
          disabled={info.disabled}
          readOnly={info.readonly}
          />
          {touched[info.name] && errors[info.name] ? (
              <div className="error-message">{errors[info.name]}</div>
          ) : null}
        </Col>
)
}