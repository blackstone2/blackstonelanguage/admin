import Form from '../forms'
import { UpdateLanguage, getLanguage } from '../../api';
import { useRouter } from 'next/router';
import ToastMessageDisplay from '../Toast/toastMessageDisplay';
import React, { useEffect, useState } from 'react';

export default function Create() {

    const router = useRouter()

    const [initialData,setInitialData] = useState([])

    const toBase64 = (file) => {
        return new Promise((resolve, reject) => {
          const fileReader = new FileReader();
      
          fileReader.readAsDataURL(file);
      
          fileReader.onload = () => {
            resolve(fileReader.result);
          };
      
          fileReader.onerror = (error) => {
            reject(error);
          };
        });
    };
    
    useEffect(() => {   
        const fetchData = async(id) => {
            await getLanguage(id)
            .then(function(res) {
                if(res && res.status == 200) {
                    setInitialData(res.data.language)
                } else {
                    
                }
            })
        }
        if(router && router.query && router.query.id)
        {
            fetchData(router.query.id)
        }
    },[router.query])

    const UpdateLanguageFunction = async(values) => {

        if(router.query && router.query.id)
        {
            if(typeof values.logo == 'object'){
                values.logo = await toBase64(values.logo);
            }
            await UpdateLanguage(values,router.query.id)
                .then(function(res) {
                    
                if(res && res.status == 200) {
                        ToastMessageDisplay({type:"success",message: res.data.message})
                        router.push('/languages')
                } else {
                        ToastMessageDisplay({type:"error",message: res.data.message})

                }
            })
        }
    }

    const formFields = [
        {
            "type" : "TextInput",
            "name" : "name",
            "label" : "Language Name",
            "placeholder" : "Enter Language Name",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "Media",
            "name" : "logo",
            "label" : "Logo",
            "placeholder" : "",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "Dropdown",
            "name" : "status",
            "label" : "Status",
            "placeholder" : "Select Status",
            "validations" : {
                "required" : true,
            },
            "options" : [
                {"key":"Active","value":"Active"},
                {"key":"InActive","value":"InActive"},
            ]
        },
    ];

    return (
        <Form info={formFields} submitAction={UpdateLanguageFunction} getInitialData={initialData} />
    )
}