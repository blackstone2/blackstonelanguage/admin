import { GetPages, DeletePage, Activate} from '../../api';
import TableWithAction from '../Datatable/table';
import React, { useEffect, useState } from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { useRouter } from 'next/router';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import ToastMessageDisplay from '../Toast/toastMessageDisplay';


export default function PageIndex() {

    const [Pages,setPages] = useState([]);
    const router = useRouter()
    
    const PageHeader = [
        {"header_name" : "Name","key_value" : "name"},
        {"header_name" : "Slug","key_value" : "slug"},
        {"header_name" : "Sequence","key_value" : "sequence"},
        {"header_name" : "Status","key_value" : "status"}
    ];
    
    const fetchPage = async(page = 1) => {
        await GetPages(page)
        .then(function(res) {
            if(res && res.status == 200) {
                setPages(res.data.details)
            } else {
                
            }
        })
    }

    const deletePage = async(id) => {
        console.log(id);
        await DeletePage(id)
        .then(function(res) {
            if(res && res.status == 200) {
                ToastMessageDisplay({type:"success",message: res.data.message})
                fetchPage();
            } else {
                
            }
        })
    }

    const activatePage = async(id) => {
        console.log(id);
        await Activate(id)
        .then(function(res) {
            if(res && res.status == 200) {
                ToastMessageDisplay({type:"success",message: res.data.message})
                fetchPage();
            } else {
                
            }
        })
    }

    

    useEffect(() => {
        fetchPage()
        
    },[])

    const actions = [
       
        {
            action_type : "action_with_popup",
            action_name : "Delete",
            action_function : deletePage
        },
        {
            action_type : "redirection",
            action_name : "Edit",
            pre_url : "/pages/edit/"
        },
        
    ];


    return (

        <Container style={{marginTop:"20px"}}>
            
            <Row>
                
                <Col>
                    <h5>Pages details</h5>
                </Col>
                <Col className="pull-right">
                    <Button onClick={() => {
                        router.push('pages/create')
                    }} style={{float:'right'}}>Add new</Button>
                </Col>
                    
                
            </Row>

            <Row >
                
                <TableWithAction header={PageHeader} data={Pages} actions={actions} onPageChangeFunction={fetchPage}/>
                
            </Row>

            
            
            {/* <PageCreate /> */}
        </Container>

    )
}