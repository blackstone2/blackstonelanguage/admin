import Layout from '../../Components/Layout'
import LanguageIndex from '../../Components/Language'

export default function Create() {

    return (
        <Layout>
            <LanguageIndex />
        </Layout>
    )
}