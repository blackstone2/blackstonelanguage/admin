import Link from "next/link";
import { Nav, Button } from 'react-bootstrap';
import React, { useEffect, useState } from 'react';
import { GetCountries } from '../../api';

export default function Footer() {

  const [userType,setUserType] = useState("Admin");
  const [databases,setDatabases] = useState([]);
  const [selectedDatabase,setSelectedDatabase] = useState([]);


  const handleDbChange = (e) => {
    console.log(e.target.value)
    localStorage.setItem('country',e.target.value);
    setSelectedDatabase(e.target.value)
    window.location.reload();
  }

  useEffect(() => {   
    if(localStorage.getItem('token') != undefined){
        
        setUserType(localStorage.getItem('type'))
    }
    else{
        
    }
    
  },[])

  useEffect(() => {   
      const fetchData = async() => {
          await GetCountries()
          .then(function(res) {
              if(res && res.status == 200) {
                  console.log(res.data.databases)
                  setDatabases(res.data.databases);
                  setSelectedDatabase(localStorage.getItem('country'))
              } else {
                  
              }
          })
      }
      fetchData()
  },[])

  return (
    <>
    <div style={{ position: "relative", bottom: 0, width:"100%",marginTop:"10px" }} className="">
      <Nav className="row navbar-dark bg-dark justify-content-between p-2 align-items-center" >

          <div className="col-8" style={{color:"white"}}>
            © Cair 4 YOUTH. All rights reserved.
            </div>

            <>
              {userType == "Super Admin" ?
                  (
                      <>
                      
                      <select className="form-control col-4" onChange={handleDbChange} style={{ width: "auto",marginRight:"10px" }} title="Change Region">
                          {Object.entries(databases) && Object.entries(databases).length > 0 &&
                              Object.entries(databases).map((db, i) => (
                                  <option value={db[0]} selected={selectedDatabase == db[0] ? true : false}>{db[0]}</option>
                              ))}

                      </select></>

                  ) : null}
            </>
      </Nav>
    </div>
    </>
  );
}
