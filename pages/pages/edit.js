import Layout from '../../Components/Layout'
import PagesEdit from '../../Components/Pages/edit'

export default function Create() {
    
    return (
        <Layout>
            <PagesEdit />
        </Layout>
    )
}