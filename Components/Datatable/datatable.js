import MUIDataTable from "mui-datatables";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faTrash, faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import React, { useEffect, useState } from 'react';

export default function TableWithAction({data,options,columns,onPageChangeFunction}) {
    const [tableData,setTableData] = useState(data.data);

    console.log("data",data)
    useEffect(() => {
        if(data && data && data.length > 0)
        {
            setTableData(data)
        }
    },[data])

    return (
        <>
            <MUIDataTable
                data={tableData}
                columns={columns}
                options={options}
                onPageChangeFunction={onPageChangeFunction}
            />
        </>
    )
}