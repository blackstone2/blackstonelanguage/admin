const getHomeData = (data) => ({
    type: 'HomePage',
    data : data
})

export default {
    getHomeData
}