import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import { useMemo, useRef, useState } from "react";
import dynamic from 'next/dynamic';
import { ImageUploadEditor } from '../../api';




export default function InputCKEditor({info,values,handleChange,touched,errors,setFieldValue,quillData}) {

    const quillRef = useRef();

      const [convertedText, setConvertedText] = useState("Some default content");

    let data = "";

    const toBase64 = (file) => {
        return new Promise((resolve, reject) => {
          const fileReader = new FileReader();
      
          fileReader.readAsDataURL(file);
      
          fileReader.onload = () => {
            resolve(fileReader.result);
          };
      
          fileReader.onerror = (error) => {
            reject(error);
          };
        });
    };

    const imageHandler = (e) => {
        const editor = quillRef.current.getEditor();
        console.log(editor)
        const input = document.createElement("input");
        input.setAttribute("type", "file");
        input.setAttribute("accept", "image/*");
        input.click();
    
        input.onchange = async () => {
          let file = input.files[0];
          if (/^image\//.test(file.type)) {
            console.log(file);
            file = await toBase64(file)
            const formData = new FormData();
            formData.append("image", file);
            const res = await ImageUploadEditor({'image':file}); // upload data into server or aws or cloudinary
            const url = res?.data?.url;
            editor.insertEmbed(editor.getSelection(), "image", url);
          } else {
            ErrorToast('You could only upload images.');
          }
        };
    }

    
    const QuillNoSSRWrapper = dynamic(
        async () => {
          const { default: RQ } = await import("react-quill");
      
          return ({ forwardedRef, ...props }) => <RQ ref={forwardedRef} {...props} />;
        },
        {
          ssr: false
        }
      );
      
      const modules = useMemo(() => ({
        toolbar: {
          handlers: { image: imageHandler,},
          container: [
            [{ header: '1' }, { header: '2' }, { font: [] }],
            [{ size: [] }],
            ['bold', 'italic', 'underline', 'strike', 'blockquote'],
            [
            { list: 'ordered' },
            { list: 'bullet' },
            { indent: '-1' },
            { indent: '+1' },
            ],
            ['link', 'image', 'video'],
            ['clean'],
          ],
        },
        clipboard: {
          matchVisual: false,
        },
      }),[]);
      /*
       * Quill editor formats
       * See https://quilljs.com/docs/formats/
       */
    const formats = [
        'header',
        'font',
        'size',
        'bold',
        'italic',
        'underline',
        'strike',
        'blockquote',
        'list',
        'bullet',
        'indent',
        'link',
        'image',
        'video',
    ];

    const handleBlur = (value) => {
      setFieldValue(info.name,data)
    }

    const [value,setValue] = useState("");
    return (

        <Col md={12}>
            <div>
                <Form.Label htmlFor={info.label}>
                {info.label}
                </Form.Label>

                <QuillNoSSRWrapper modules={modules} forwardedRef={quillRef}  formats={formats} theme="snow" value={quillData[info.name]} 
                    onChange={(e) => {
                      quillData[info.name] = e;
                    }}
                    
                />
                
            </div>      
            {touched[info.name] && errors[info.name] ? (
              <div className="error-message">{errors[info.name]}</div>
          ) : null}
        </Col>
        
    )
}