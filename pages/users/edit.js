import Layout from '../../Components/Layout'
import UserEdit from '../../Components/User/edit'

export default function Create() {
    
    return (
        <Layout>
            <UserEdit />
        </Layout>
    )
}