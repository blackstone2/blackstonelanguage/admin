import Layout from '../../Components/Layout'
import LanguageCreate from '../../Components/Language/create'
export default function Create() {

    return (
        <Layout>
            <LanguageCreate />
        </Layout>
    )
}