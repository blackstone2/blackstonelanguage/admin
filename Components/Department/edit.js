import Form from '../forms'
import { UpdateDepartment, GetDepartment, GetLanguages } from '../../api';
import { useRouter } from 'next/router';
import ToastMessageDisplay from '../Toast/toastMessageDisplay';
import React, { useEffect, useState } from 'react';

export default function Create() {

    const router = useRouter()
    const [options,setOptions] = useState([])
    const [initialData,setInitialData] = useState([])
    const [type,setType] = useState(false)

    const toBase64 = (file) => {
        return new Promise((resolve, reject) => {
          const fileReader = new FileReader();
      
          fileReader.readAsDataURL(file);
      
          fileReader.onload = () => {
            resolve(fileReader.result);
          };
      
          fileReader.onerror = (error) => {
            reject(error);
          };
        });
    };
    useEffect(() => {   
        const fetchData = async(id) => {
            await GetDepartment(id)
            .then(function(res) {
                if(res && res.status == 200) {
                    setInitialData(res.data.department)
                } else {
                    
                }
            })

            await GetLanguages()
            .then(function(res) {
                if(res && res.status == 200) {
                    let arr = [];
                    
                    res.data.details.map((lang) => {
                        arr.push({"key":lang.name ,"value":lang.name})
                        setOptions(arr)
                        
                    })
                } else {
                    
                }
            })
        }
        if(router && router.query && router.query.id)
        {
            fetchData(router.query.id)

            if (typeof window !== 'undefined') {

                if(localStorage.getItem('type') == "Super Admin")
                {
                    setType(false)
                }
                else{
                    setType(true)
                }
            }
        }
    },[router.query])

    const UpdateDepartmentFunction = async(values) => {

        if(router.query && router.query.id)
        {
            console.log(typeof values.logo);
            if(typeof values.logo == 'object'){
                values.logo = await toBase64(values.logo);
            }
            await UpdateDepartment(values,router.query.id)
                .then(function(res) {
                    
                if(res && res.status == 200) {
                        ToastMessageDisplay({type:"success",message: res.data.message})
                        router.push('/quests')
                } else {
                        ToastMessageDisplay({type:"error",message: res.data.message})

                }
            })
        }
    }

    const formFields = [
        {
            "type" : "TextInput",
            "name" : "name",
            "label" : "Quests Name",
            "placeholder" : "Enter Quests Name",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "Dropdown",
            "name" : "language",
            "label" : "Language Name",
            "placeholder" : "Select Language Name",
            "validations" : {
                "required" : true,
            },
            "options" : options,
            "disabled" : type
        },
        {
            "type" : "Media",
            "name" : "logo",
            "label" : "Logo",
            "placeholder" : "",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "Dropdown",
            "name" : "status",
            "label" : "Status",
            "placeholder" : "Select Status",
            "validations" : {
                "required" : true,
            },
            "options" : [
                {"key":"Active","value":"Active"},
                {"key":"InActive","value":"InActive"},
            ]
        },
    ];

    return (
        <Form info={formFields} submitAction={UpdateDepartmentFunction} getInitialData={initialData} />
    )
}