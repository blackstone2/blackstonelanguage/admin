import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';

export default function DependantDropdown({info,values,handleChange,touched,errors}) {
    return (
        <Col xs={12} md={6}>
            
            <label htmlFor={info.label} >
            {info.label}
            </label>
            <Form.Select
                id={info.name}
                name={info.name}
                autoComplete={info.name}
                value={values[info.name]}
                onChange={handleChange}
                className={touched[info.name] && errors[info.name] ? "mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm error" : "mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"}
                disabled={info.disabled}
                readonly={info.readonly}
            >{
                <>
                    <option value="">{info.placeholder}</option>
                    {
                        
                        info.fetch_Function(values[info.Dependent_field]) && Object.keys(info.fetch_Function(values[info.Dependent_field])).length > 0 && info.fetch_Function(values[info.Dependent_field]).map((option) => (
                            <option value={option.value}>{option.key}</option>
                        ))
                    }
                </>
            }
            </Form.Select>
            {touched[info.name] && errors[info.name] ? (
                <div className="error-message">{errors[info.name]}</div>
            ) : null}
        </Col>
        
    )
}