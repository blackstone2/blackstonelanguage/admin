import { Field } from 'formik';
export default function RadioGroup({info,values,handleChange,touched,errors}) {
    return (
        <div className="md:col-span-1">
            <label htmlFor={info.label} className="block text-sm font-medium text-gray-700">
                {info.label}
            </label>
            <div role="group" aria-labelledby="my-radio-group">
                {
                    info.options && info.options.map((option) => (
                        <label style={{margin:"10px"}}>
                            <Field type="radio" name={info.name} value={option.value} />
                            {option.key}
                        </label>        
                    ))
                }
                
            
            </div>
        </div>
        
      
    )
  }