const initialState = { 
    data: [] 
}

const BlogReducer = (state = initialState, action) => {
  console.log(action)
    switch (action.type) {
      case 'BlogPage':
        return {
          ...state,
          data : action.data
        }
      default:
        return state
    }
}

export default BlogReducer