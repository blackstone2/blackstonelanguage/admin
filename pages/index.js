import React, { useEffect, useState } from 'react';
import Layout from '../Components/Layout'
import Container from 'react-bootstrap/Container';
import { useRouter } from 'next/router';

export default function Dashboard() {
    const router = useRouter()

    useEffect(() => {   
        if(localStorage.getItem('token') == undefined){
            router.push('/auth/login');
        }
        
    },[])
    return (
        <Layout>
            <Container style={{marginTop:"20px"}}>
                <h1>Welcome!</h1>
            </Container>
        </Layout>
    )
}