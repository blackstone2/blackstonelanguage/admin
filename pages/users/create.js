import Layout from '../../Components/Layout'
import UserCreate from '../../Components/User/create'
export default function Create() {

    return (
        <Layout>
            <UserCreate />
        </Layout>
    )
}