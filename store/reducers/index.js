import { combineReducers } from 'redux'

import HomeReducer from './homeReducer'
// import HomeReducer from './homeReducer'

const rootReducer = combineReducers({
  HomeReducer
})

export default rootReducer