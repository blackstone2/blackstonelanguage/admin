import { GetCourses, DeleteCourse} from '../../api';
import TableWithAction from '../Datatable/table';
import React, { useEffect, useState } from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { useRouter } from 'next/router';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import ToastMessageDisplay from '../Toast/toastMessageDisplay';

export default function CourseIndex() {

    const [courses,setCourses] = useState([]);
    const router = useRouter()
    
    const courseHeader = [
        {"header_name" : "Course Name","key_value" : "name"},
        {"header_name" : "Language","key_value" : "language"},
        {"header_name" : "Status","key_value" : "status"}
    ];
    
    const fetchCourse = async(page = 1) => {
        await GetCourses(page)
        .then(function(res) {
            if(res && res.status == 200) {
                setCourses(res.data.details)
            } else {
                
            }
        })
    }

    const deleteCourse = async(id) => {
        console.log(id);
        await DeleteCourse(id)
        .then(function(res) {
            if(res && res.status == 200) {
                ToastMessageDisplay({type:"success",message: res.data.message})
                fetchCourse();
            } else {
                
            }
        })
    }

    useEffect(() => {
        fetchCourse()
        
    },[])

    const actions = [
        {
            action_type : "redirection",
            action_name : "Edit",
            pre_url : "/courses/edit/"
        },
        {
            action_type : "action_with_popup",
            action_name : "Delete",
            action_function : deleteCourse
        }
    ];


    return (

        <Container style={{marginTop:"20px"}}>
            <Row>
                
                <Col>
                    <h5>Course details</h5>
                </Col>
                <Col className="pull-right">
                    <Button onClick={() => {
                        router.push('courses/create')
                    }} style={{float:'right'}}>Add new</Button>
                </Col>
                    
                
            </Row>

            <Row >
                
                <TableWithAction header={courseHeader} data={courses} actions={actions} onPageChangeFunction={fetchCourse}/>
                
            </Row>

            
            
            {/* <CourseCreate /> */}
        </Container>

    )
}