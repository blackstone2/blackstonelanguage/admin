import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import { Formik, Field, FieldArray, ErrorMessage } from 'formik';
import { Login, GetCountries } from '../api';
import * as Yup from "yup";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import InputGroup from 'react-bootstrap/InputGroup';
import Form from 'react-bootstrap/Form';
import { toast } from "react-toastify";
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';

const validationSchema = Yup.object().shape({
    email: Yup.string().email('Invalid email address').required('Required'),
    password: Yup.string().required('Required')
    
});



function AuthLogin() {

    const handleDbChange = (e) => {
        console.log(e.target.value)
        localStorage.setItem('country',e.target.value);
        // setSelectedDatabase(e.target.value)
        // window.location = '/';
    }

    const [databases,setDatabases] = useState([]);
    useEffect(() => {   
        const fetchData = async() => {
            await GetCountries()
            .then(function(res) {
                if(res && res.status == 200) {
                    console.log(res.data.databases)
                    setDatabases(res.data.databases);
                } else {
                    
                }
            })
        }
        fetchData()
    },[])
    const router = useRouter();

    return (
        
        <Container style={{marginTop:"20px"}}>

            <Formik
                enableReinitialize="true"
                initialValues={{
                    email: "",
                    password: "",
                    portal:"Admin"
                  }}
                  validationSchema={validationSchema}
                  onSubmit={async (values) => {
                    await Login(values).then(function (res) {
                      if (res && res.status == 200) {
                        console.log(res.data.token);
                        localStorage.setItem('token',res.data.token);
                        localStorage.setItem('type',res.data.type);
                        toast.success("Logged In Successfully");
                        router.push({
                          pathname: '/'
                        });
                      } else {
                        console.log(res);
                        toast.error(res.data.error);
                        
                      }
                    })
                    
        
                  }}
            >
                {({ handleSubmit, handleChange, values, setFieldValue, errors, touched }) => (
                    <form onSubmit={handleSubmit}>
                    
                          
                        <Row>
                            <Col xs={{ span: 4, offset: 4 }} md={{ span: 4, offset: 4 }}>
                                <h4 style={{fontWeight:"700",textAlign:"left",marginTop:"50px"}}>Welcome To Admin</h4>  
                            </Col>
                            
                            <Col xs={{ span: 4, offset: 4 }} md={{ span: 4, offset: 4 }} style={{marginTop:"50px"}}>
                            <div style={{ marginTop: '10px' }}>
                                <InputGroup fullWidth size="Small">
                                <Form.Control autoFocus="true" id="email" name="email" type="email" onChange={handleChange} autoComplete="off"
                                    value={values.email} placeholder="Email" className={touched.email && errors.email ? "error" : null} />
                                </InputGroup>
                                {touched.email && errors.email ? (
                                <div className="error-message">{errors.email}</div>
                                ) : null}
                            </div>
                            </Col><br /><br />
                        </Row>
                        <Row>
                            
                            <Col xs={{ span: 4, offset: 4 }} md={{ span: 4, offset: 4 }}>
                            <div style={{ marginTop: '20px' }}>
                                <InputGroup fullWidth size="Small">
                                <Form.Control id="password" name="password" type="password" onChange={handleChange} autoComplete="off"
                                    value={values.password} placeholder="Password" className={touched.password && errors.password ? "error" : null} />
                                </InputGroup>
                                {touched.password && errors.password ? (
                                <div className="error-message">{errors.password}</div>
                                ) : null}
                            </div>
                            </Col>
                        </Row>
                        
                        <Row>
                            
                            <Col xs={{ span: 4, offset: 4 }} md={{ span: 4, offset: 4 }}>
                            <div style={{ marginTop: '20px' }}>
                                <label style={{marginTop:"20px",marginBottom:"10px"}}> Select Region </label>
                                <select className="form-control" onChange={handleDbChange}> 
                                {
                                    Object.entries(databases) && Object.entries(databases).length > 0 &&
                                    Object.entries(databases).map((db,i) => (
                                        <option value={db[0]} >{db[0]}</option>
                                    ))
                                    
                                }
                                
                                </select>
                            </div>
                            </Col>
                        </Row>
                        
                        <Row>
                            
                            <Col xs={{ span: 4, offset: 4 }} md={{ span: 4, offset: 4 }}>
                            <div style={{ marginTop: '10px' }}>
                                <Button status="Success" type="submit" className="mppButton" onClick={handleSubmit}>
                                SIGN IN 
                                </Button>
                            </div>
                            </Col>
                            
                        </Row>
                                
                              
                  </form>
                )}
            </Formik>
            
        </Container>
    );
}

export default AuthLogin;
