import React from 'react';
import { Provider } from 'react-redux'
import store from "../store";
import "../styles/globals.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect } from 'react';
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import 'react-quill/dist/quill.snow.css'

export default function App({ Component, pageProps }) {

	useEffect(() => {
		import('bootstrap/dist/js/bootstrap');
	  }, []);
	return (
        <Provider store={store}>
            <Component {...pageProps} />
			<ToastContainer
				position="top-right"
				autoClose={1000}
				hideProgressBar={false}
				newestOnTop={false}
				draggable={false}
				pauseOnVisibilityChange
				closeOnClick
				pauseOnHover
			/>
	    </Provider>
		
	);
}