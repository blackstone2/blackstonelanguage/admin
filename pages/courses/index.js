import Layout from '../../Components/Layout'
import CourseIndex from '../../Components/Course'

export default function Create() {

    return (
        <Layout>
            <CourseIndex />
        </Layout>
    )
}