import { GetUsers, DeleteUser, Activate} from '../../api';
import TableWithAction from '../Datatable/table';
import React, { useEffect, useState } from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { useRouter } from 'next/router';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import ToastMessageDisplay from '../Toast/toastMessageDisplay';


export default function UserIndex() {

    const [users,setUsers] = useState([]);
    const router = useRouter()
    
    const userHeader = [
        {"header_name" : "Name","key_value" : "name"},
        {"header_name" : "Email","key_value" : "email"},
        {"header_name" : "Type","key_value" : "type"},
        {"header_name" : "Status","key_value" : "status"}
    ];
    
    const fetchUser = async(page = 1) => {
        await GetUsers(page)
        .then(function(res) {
            if(res && res.status == 200) {
                setUsers(res.data.details)
            } else {
                
            }
        })
    }

    const deleteUser = async(id) => {
        console.log(id);
        await DeleteUser(id)
        .then(function(res) {
            if(res && res.status == 200) {
                ToastMessageDisplay({type:"success",message: res.data.message})
                fetchUser();
            } else {
                
            }
        })
    }

    const activateUser = async(id) => {
        console.log(id);
        await Activate(id)
        .then(function(res) {
            if(res && res.status == 200) {
                ToastMessageDisplay({type:"success",message: res.data.message})
                fetchUser();
            } else {
                
            }
        })
    }

    

    useEffect(() => {
        fetchUser()
        
    },[])

    const actions = [
        {
            action_type : "action_with_popup",
            action_name : "Activate",
            action_function : activateUser
        },
        {
            action_type : "action_with_popup",
            action_name : "Delete",
            action_function : deleteUser
        },
        {
            action_type : "redirection",
            action_name : "Edit",
            pre_url : "/users/edit/"
        },
        
    ];


    return (

        <Container style={{marginTop:"20px"}}>
            
            <Row>
                
                <Col>
                    <h5>Users details</h5>
                </Col>
                <Col className="pull-right">
                    <Button onClick={() => {
                        router.push('users/create')
                    }} style={{float:'right'}}>Add new</Button>
                </Col>
                    
                
            </Row>

            <Row >
                
                <TableWithAction header={userHeader} data={users} actions={actions} onPageChangeFunction={fetchUser}/>
                
            </Row>

            
            
            {/* <UserCreate /> */}
        </Container>

    )
}