import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';


export default function TextArea({info,values,handleChange,touched,errors,setFieldValue}) {
    return (

        <Col md={12}>
          <Form.Label htmlFor={info.label}>
            {info.label}
          </Form.Label>
          <Form.Control
            as="textarea" rows={3}
            name={info.name}
            placeholder={info.placeholder}
            value={values[info.name]}
            id={info.label}
            onChange={handleChange}
            autoComplete={info.name}
            
            disabled={info.disabled}
            readonly={info.readonly}
          />
          {touched[info.name] && errors[info.name] ? (
              <div className="error-message">{errors[info.name]}</div>
          ) : null}
        </Col>
        
    )
}