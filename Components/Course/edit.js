import Form from '../forms'
import { UpdateCourse, GetDepartments, GetCourse } from '../../api';
import { useRouter } from 'next/router';
import ToastMessageDisplay from '../Toast/toastMessageDisplay';
import React, { useEffect, useState } from 'react';

export default function Create() {

    const router = useRouter()

    const toBase64 = (file) => {
        return new Promise((resolve, reject) => {
          const fileReader = new FileReader();
      
          fileReader.readAsDataURL(file);
      
          fileReader.onload = () => {
            resolve(fileReader.result);
          };
      
          fileReader.onerror = (error) => {
            reject(error);
          };
        });
    };

    const [initialData,setInitialData] = useState([])
    const [options,setOptions] = useState([])
    const [departments,setDepartments] = useState([])
    
    useEffect(() => {   
        const fetchData = async(id) => {
            await GetCourse(id)
            .then(function(res) {
                if(res && res.status == 200) {
                    setInitialData(res.data.course)
                } else {
                    
                }
            })
        }
        if(router && router.query && router.query.id)
        {
            fetchData(router.query.id)
        }
    },[router.query])

    const UpdateCourseFunction = async(values) => {

        if(router.query && router.query.id)
        {
            if(typeof values.logo == 'object'){
                values.logo = await toBase64(values.logo);
            }
            await UpdateCourse(values,router.query.id)
                .then(function(res) {
                    
                if(res && res.status == 200) {
                        ToastMessageDisplay({type:"success",message: res.data.message})
                        router.push('/courses')
                } else {
                        ToastMessageDisplay({type:"error",message: res.data.message})

                }
            })
        }
    }

    useEffect(() => {   
        const fetchData = async() => {
            await GetDepartments()
            .then(function(res) {
                if(res && res.status == 200) {
                    let arr = [];
                    setDepartments(res.data.details);
                    res.data.details.map((dept) => {
                        arr.push({"key":dept.name + ' - '+ dept.language ,"value":dept._id})
                        setOptions(arr)
                        
                    })
                } else {
                    
                }
            })
        }
        
        fetchData()
        
    },[])

    const getCategory = (department_id) => {
        let selectedDepartment = departments.filter((dept) => { return dept._id == department_id})
        let arr = [];
        selectedDepartment && selectedDepartment[0] && selectedDepartment[0].categories && selectedDepartment[0].categories.map((category) => {
            arr.push({"key":category.name,"value":category.name})
        })
        return arr;
    }

    const formFields = [
        {
            "type" : "TextInput",
            "name" : "name",
            "label" : "Course Name",
            "placeholder" : "Enter Course Name",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "Media",
            "name" : "logo",
            "label" : "Logo",
            "placeholder" : "",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "Dropdown",
            "name" : "department_id",
            "label" : "Quests",
            "placeholder" : "Select Quests",
            "validations" : {
                "required" : true,
            },
            "options" : options
        },
        {
            "type" : "DependantDropdown",
            "name" : "category",
            "label" : "Category",
            "placeholder" : "Select Category",
            "validations" : {
                "required" : true,
            },
            "options" : [],
            "options_type" : "Dependent",
            "Dependent_field" : "department_id",
            "fetch_Function" : getCategory
        },
        
        {
            "type" : "Dropdown",
            "name" : "status",
            "label" : "Status",
            "placeholder" : "Select Status",
            "validations" : {
                "required" : true,
            },
            "options" : [
                {"key":"Active","value":"Active"},
                {"key":"InActive","value":"InActive"},
            ]
        },
        {
            "type" : "TextArea",
            "name" : "shortDescription",
            "label" : "Short Description",
            "placeholder" : "Enter Short Description",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "TextArea",
            "name" : "introduction",
            "label" : "Introduction",
            "placeholder" : "Enter Introduction",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "TextArea",
            "name" : "task",
            "label" : "Task",
            "placeholder" : "Enter Task",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "TextArea",
            "name" : "process",
            "label" : "Process",
            "placeholder" : "Enter Process",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "TextArea",
            "name" : "conclusion",
            "label" : "Conclusion",
            "placeholder" : "Enter Conclusion",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "TextArea",
            "name" : "learningObjectives",
            "label" : "Learning Objectives",
            "placeholder" : "Enter Learning Objectives",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "TextArea",
            "name" : "educator_details",
            "label" : "Educator Details",
            "placeholder" : "Enter Educator Details",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "TextArea",
            "name" : "links",
            "label" : "Links",
            "placeholder" : "Links",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "TextArea",
            "name" : "documents",
            "label" : "Documents",
            "placeholder" : "Documents",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "TextArea",
            "name" : "videos",
            "label" : "Videos",
            "placeholder" : "Videos",
            "validations" : {
                "required" : true,
            },
        },
        {
            "type" : "BasicTextArea",
            "name" : "quiz",
            "label" : "Quiz",
            "placeholder" : "Quiz",
            "validations" : {
                "required" : true,
            },
        },
    ];

    return (
        <Form info={formFields} submitAction={UpdateCourseFunction} getInitialData={initialData} />
    )
}