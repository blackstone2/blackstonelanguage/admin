import { GetLanguages, DeleteLanguage} from '../../api';
import TableWithAction from '../Datatable/table';
import React, { useEffect, useState } from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { useRouter } from 'next/router';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import ToastMessageDisplay from '../Toast/toastMessageDisplay';


export default function LanguageIndex() {

    const [languages,setLanguages] = useState([]);
    const router = useRouter()
    
    const languageHeader = [
        {"header_name" : "Language Name","key_value" : "name"},
        {"header_name" : "Status","key_value" : "status"}
    ];
    
    const fetchLanguage = async(page = 1) => {
        await GetLanguages(page)
        .then(function(res) {
            if(res && res.status == 200) {
                setLanguages(res.data.details)
            } else {
                
            }
        })
    }

    const deleteLanguage = async(id) => {
        console.log(id);
        await DeleteLanguage(id)
        .then(function(res) {
            if(res && res.status == 200) {
                ToastMessageDisplay({type:"success",message: res.data.message})
                fetchLanguage();
            } else {
                
            }
        })
    }

    useEffect(() => {
        fetchLanguage()
        
    },[])

    const actions = [
        {
            action_type : "redirection",
            action_name : "Edit",
            pre_url : "/languages/edit/"
        },
        {
            action_type : "action_with_popup",
            action_name : "Delete",
            action_function : deleteLanguage
        }
    ];


    return (

        <Container style={{marginTop:"20px"}}>
            
            <Row>
                <Col>
                    <h5>Language details</h5>
                </Col>
                <Col className="pull-right">
                    <Button onClick={() => {
                        router.push('languages/create')
                    }} style={{float:'right'}}>Add new</Button>
                </Col>
                    
                
            </Row>

            <Row >
                
                <TableWithAction header={languageHeader} data={languages} actions={actions} onPageChangeFunction={fetchLanguage}/>
                
            </Row>

            
            
            {/* <LanguageCreate /> */}
        </Container>

    )
}