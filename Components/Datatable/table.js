import Table from 'react-bootstrap/Table';
import React, { useEffect, useState } from 'react';
import { FaTrashAlt, FaEdit, FaCheck } from "react-icons/fa";


export default function TableWithAction({header,data,actions,onPageChangeFunction}) {
    const [tableData,setTableData] = useState(data);
    const [currentPage,setPage] = useState(1);
    const [pageCount,setPageCount] = useState(10);

    useEffect(() => {
        console.log(data);
        if(data && data.length > 0)
        {
            setTableData(data);
            console.log(data);
        }
    },[data])

    const onPageChange = (page) => {
        
        
    };

    return (
        <>
            <Table striped bordered hover responsive className='mt-3'>
                <thead>
                    <tr>
                    {
                        header && header.map((header_details) => (
                            <th>
                                {header_details.header_name}
                            </th>        
                        ))
                        
                    }
                    {
                        actions && actions.map((action) => (
                            <th>
                                {action.action_name}
                            </th>  
                                
                        ))
                    }
                    </tr>
                    
                </thead>
                <tbody>
                    {
                        tableData && tableData.map((row_data) => (
                            <tr>
                                {
                                    <>
                                    {
                                        header && header.map((header_details) => (
                                            <td>
                                                {row_data[header_details.key_value]}
                                            </td>
                                        ))
                                    }
                                    {
                                        actions && actions.map((action) => (
                                            <>
                                            {
                                                action.action_type == "redirection" &&
                                                    <td>
                                                        <a
                                                            href={`${action.pre_url}?id=${row_data._id}`}
                                                            
                                                        >
                                                            <FaEdit style={{color:"black"}}/>
                                                        </a>
                                                    </td>
                                            }
                                            {
                                                action.action_type == "action" &&
                                                    <td>
                                                        <button
                                                            onClick={() => {
                                                                action.action_function(row_data.id)
                                                                onPageChange(currentPage)
                                                            }}
                                                            
                                                        >
                                                            {action.action_name}
                                                        </button>
                                                    </td>
                                            }
                                            {
                                                action.action_type == "action_with_popup" &&
                                                    <td>
                                                        <button style={{padding: 0,border: "none",background: "none"}}
                                                            onClick={() => {
                                                                const confirmBox = window.confirm(`Do you really want to ${action.action_name} ?`
                                                                )
                                                                if (confirmBox === true) {
                                                                    action.action_function(row_data._id)
                                                                    onPageChange(currentPage)
                                                                }
                                                            }}
                                                            
                                                        >
                                                            {
                                                                action.action_name == "Delete" ? <FaTrashAlt/> : null
                                                            }
                                                            {
                                                                row_data.type=="Teacher" && row_data.status=="InActive" && action.action_name == "Activate" ? <><FaCheck/> Activate</> : null
                                                            }
                                                            
                                                        </button>
                                                    </td>
                                            }
                                            
                                            </>
                                        ))
                                    }
                                    
                                    </>
                                }
                            </tr>        
                        ))
                    }
                    
                </tbody>
            </Table>
            {/* {
                tableData && tableData.length > 0 && 
                    // <Pagination
                    //     currentPage={currentPage}
                    //     totalPages={pageCount}
                    //     onPageChange={onPageChange}

                    // />
            } */}
            
        </>
    )
}